#!/bin/sh

# Build and link.
gcc \
 src/Main.c \
 src/Util.c \
 src/Globals.c \
 src/Vector.c \
 src/BlockingQueue.c \
 src/Types.c \
 src/JsonParser.c \
 src/ArrayList.c \
-Ofast -std=c99 -Iinc/SDL -Llib/SDL -Wl,-rpath,lib/SDL -lSDL -lpthread

# Copy the result to the bin/ directory.
mv a.out bin/armrender.out
