#include <stdlib.h>
#include <stdio.h>

#include "ArrayList.h"

#define DEFAULT_ARRAY_SIZE 10

ArrayList NewArrayList()
{
    ArrayList arrayList;
    
    arrayList.items = malloc(sizeof(void *) * DEFAULT_ARRAY_SIZE);
    arrayList.itemCount = 0;
    arrayList.arraySize = DEFAULT_ARRAY_SIZE;
    
    return arrayList;
}

void DisposeArrayList(ArrayList * arrayList)
{
    free(arrayList->items);
}

void DeepDisposeArrayList(ArrayList * arrayList)
{
    for (unsigned int i = 0; i < arrayList->itemCount; i++)
    {
        free(ArrayListGetDangerous(arrayList, i));
    }

    DisposeArrayList(arrayList);
}

void ArrayListAdd(ArrayList * arrayList, void * item)
{
    if (arrayList->itemCount == arrayList->arraySize)
    {
        // Double the array size.
        unsigned int newArraySize = arrayList->arraySize * 2;
        
        void ** newItems = malloc(sizeof(void *) * newArraySize);
        
        // Copy over the old array.
        for (unsigned int i = 0; i < arrayList->itemCount; i++)
        {
            newItems[i] = arrayList->items[i];
        }
        
        // Delete the old array.
        free(arrayList->items);
        
        // Update the members.
        arrayList->items = newItems;
        arrayList->arraySize = newArraySize;
    }
    
    // Here, we are guaranteed that we can add the item.
    arrayList->items[arrayList->itemCount] = item;
    arrayList->itemCount = arrayList->itemCount + 1;
}

void * ArrayListGet(ArrayList * arrayList, unsigned int index)
{
    // Do bounds checking.
    if (index < 0)
    {
        fprintf(stderr, "ArrayListGet on negative index: %d\n", index);
        exit(1);
    }
    
    if (index >= arrayList->itemCount)
    {
        fprintf(stderr, "ArrayListGet on out of bounds index: %d\n", index);
        exit(1);
    }

    // Call method that gets without bounds checking.
    return ArrayListGetDangerous(arrayList, index);
}

void * ArrayListGetDangerous(ArrayList * arrayList, unsigned int index)
{
    return arrayList->items[index];
}
