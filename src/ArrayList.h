#ifndef _ARMRENDER_ARRAY_LIST_H_
#define _ARMRENDER_ARRAY_LIST_H_

typedef struct
{
    void ** items;
    unsigned int itemCount;
    unsigned int arraySize;
} 
ArrayList;

// Creates a new ArrayList.
ArrayList NewArrayList();

// Creates a new ArrayList.
void DisposeArrayList(ArrayList * arrayList);
void DeepDisposeArrayList(ArrayList * arrayList);

// Adds an item.
void ArrayListAdd(ArrayList * arrayList, void * item);

// Does bounds checking.
void * ArrayListGet(ArrayList * arrayList, unsigned int index);

// Doesn't do bounds checking.
void * ArrayListGetDangerous(ArrayList * arrayList, unsigned int index);

#endif
