#include "BlockingQueue.h"

void * BlockingQueueRemove(BlockingQueue * queue)
{
    // Grab the lock.
    pthread_mutex_lock(&(queue->mutex));
    
    // If we have no items, wait for one to be added.
    while ((queue->halting != 1) && (queue->numItems == 0))
    {
        pthread_cond_wait(&(queue->itemAdded), &(queue->mutex));
    }
    
    // Check if we are halting threads.
    if (queue->halting == 1)
    {
        // Release the lock and exit.
        pthread_mutex_unlock(&(queue->mutex));
        pthread_exit(NULL);
    }
    
    // Grab the item.
    void * item = (queue->items)[queue->head];
    
    // Increment head (wraparound if needed - this is a circular array).
    queue->head = queue->head + 1;
    
    if (queue->head >= queue->maxItems)
    {
        queue->head = 0;
    }
    
    // Decrement the number of items.
    queue->numItems = queue->numItems - 1;
    
    // Signal to the producer that we have taken an item.
    pthread_cond_signal(&(queue->itemRemoved));
    
    // Release the lock.
    pthread_mutex_unlock(&(queue->mutex));
    
    return item;
}

void BlockingQueueAdd(BlockingQueue * queue, void * item)
{
    // Grab the lock.
    pthread_mutex_lock(&(queue->mutex));
    
    // If we have a full queue, wait for an item to be removed.
    // If we are halting this thread, then stop waiting.
    while ((queue->halting != 1) && queue->numItems == queue->maxItems)
    {
        pthread_cond_wait(&(queue->itemRemoved), &(queue->mutex));
    }
    
    // Check if we are halting threads.
    if (queue->halting == 1)
    {
        // Release the lock and exit.
        pthread_mutex_unlock(&(queue->mutex));
        pthread_exit(NULL);
    }
    
    // Put the item in.
    (queue->items)[queue->tail] = item;
    
    // Increment tail (wraparound if needed - this is a circular array).
    queue->tail = queue->tail + 1;
    
    if (queue->tail >= queue->maxItems)
    {
        queue->tail = 0;
    }
    
    // Increment the number of items.
    queue->numItems = queue->numItems + 1;
    
    // Signal to the consumer that we have added an item.
    pthread_cond_signal(&(queue->itemAdded));
    
    // Release the lock.
    pthread_mutex_unlock(&(queue->mutex));
}

BlockingQueue NewBlockingQueue(int maxItems)
{
    BlockingQueue queue;
    
    queue.head = 0;
    queue.tail = 0;
    
    queue.halting = 0;
    
    queue.numItems = 0;
    queue.maxItems = maxItems;
    
    queue.items = (void **) malloc(sizeof(void *) * maxItems);
    
    pthread_mutex_init(&(queue.mutex), NULL);
    
    pthread_cond_init(&(queue.itemAdded), NULL);
    pthread_cond_init(&(queue.itemRemoved), NULL);
    
    return queue;
}

void AlertBlockingQueueForHalt(BlockingQueue * queue)
{
    // Grab the lock.
    pthread_mutex_lock(&(queue->mutex));
    
    // Set flag indicating that we are going to halt.
    queue->halting = 1;
    
    // Signal (broadcast) all producers and consumers.
    pthread_cond_broadcast(&(queue->itemAdded));
    pthread_cond_broadcast(&(queue->itemRemoved));
    
    // Release the lock.
    pthread_mutex_unlock(&(queue->mutex));
}

void DisposeBlockingQueue(BlockingQueue * queue)
{
    free(queue->items);

    pthread_mutex_destroy(&(queue->mutex));
    
    pthread_cond_destroy(&(queue->itemAdded));
    pthread_cond_destroy(&(queue->itemRemoved));
}

QueuePair NewQueuePair(BlockingQueue * queueA, BlockingQueue * queueB)
{
    QueuePair pair;
    
    pair.queueA = queueA;
    pair.queueB = queueB;
    
    return pair;
}
