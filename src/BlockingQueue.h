#ifndef _ARMRENDER_BLOCKING_QUEUE_H_
#define _ARMRENDER_BLOCKING_QUEUE_H_

#include <pthread.h>
#include <stdlib.h>

typedef struct 
{
    int head;
    int tail;
    
    int numItems;
    int maxItems;
    
    int halting;
    
    void ** items;
    
    pthread_cond_t itemAdded;
    pthread_cond_t itemRemoved;
    
    pthread_mutex_t mutex;
} 
BlockingQueue;

typedef struct
{
    BlockingQueue * queueA;
    BlockingQueue * queueB;
}
QueuePair;

// Constructors.
BlockingQueue NewBlockingQueue(int maxItems);
QueuePair NewQueuePair(BlockingQueue * queueA, BlockingQueue * queueB);

// Destructor.
void AlertBlockingQueueForHalt(BlockingQueue * queue);
void DisposeBlockingQueue(BlockingQueue * queue);

// Functions.
void * BlockingQueueRemove(BlockingQueue * queue);
void BlockingQueueAdd(BlockingQueue * queue, void * item);

#endif
