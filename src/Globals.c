#include <unistd.h>
#include <math.h>
#include <string.h>

#include "Globals.h"
#include "JsonParser.h"

SDL_Surface * SCREEN;

unsigned int SCREEN_WIDTH;
unsigned int SCREEN_HEIGHT;

unsigned int SHADOWING_MODE;

unsigned int LIGHTING_MODEL;

unsigned int PHONG_SPECULAR_ENABLED;
unsigned int PHONG_DIFFUSE_ENABLED;

unsigned int REFLECTION_ENABLED;
unsigned int REFRACTION_ENABLED;

unsigned int NUMBER_OF_CORES;

unsigned int PIXELS_PER_WORK_UNIT;

unsigned int RECURSION_DEPTH;

Vector BACKGROUND_COLOR;
Vector FOREGROUND_COLOR;

ArrayList * SCENE_OBJECTS;
ArrayList * SCENE_LIGHTS;

unsigned int PRINT_PERF_INFO;

// Camera Info
Vector CAMERA_POSITION;
Vector CAMERA_LOOK_AT;
Vector CAMERA_UP;

// Marterial / Light info
ArrayList * SCENE_MATERIAL_INFOS;
ArrayList * SCENE_LIGHT_INFOS;

float CAMERA_NEAR;
float CAMERA_FOV_Y;

// Pixel plane data.
Vector pixelPlaneX;
Vector pixelPlaneY;

float pixelPlaneWidth;
float pixelPlaneHeight;

Vector pixelPlaneOrigin;

float halfPixelWidth;
float halfPixelHeight;

float pixelWidth;
float pixelHeight;

// Other initializers.
void InitSdl(void);
void InitScene(JsonObject * materials, JsonObject * lights, JsonArray * objects);
void InitView(JsonObject * view);

// Helper functions.
Vector ParseVectorFromJsonValue(JsonValue * jsonValue);
MaterialInfo * GetMaterialInfo(JsonObject * jsonObject);
LightInfo * GetLightInfo(JsonObject * jsonObject);

// Helpful constants.
double DEG_TO_RAD = 3.14159265 / 180;

void InitGlobals(void)
{
    char * sceneFile = "../cfg/Scene.json";

    FILE * file = fopen(sceneFile, "r");

    if (file == NULL)
    {
        fprintf(stderr, "Could not open file: %s\n", sceneFile);
    }

    JsonObject * topLevelObject = ParseJson(file)->value;
    
    JsonObject * materials = ObjectEntryByName(topLevelObject, "MATERIALS")->value;
    JsonObject * lights    = ObjectEntryByName(topLevelObject, "LIGHTS")->value;
    JsonObject * view      = ObjectEntryByName(topLevelObject, "VIEW")->value;
    JsonArray * objects    = ObjectEntryByName(topLevelObject, "OBJECTS")->value;        

    // Set the resolution.
    SCREEN_WIDTH  = IntegerValue(ObjectEntryByName(view, "SCREEN WIDTH"));
    SCREEN_HEIGHT = IntegerValue(ObjectEntryByName(view, "SCREEN HEIGHT"));

    // Set up SDL.
    InitSdl();

    // Enable/disable printing of perf info.
    PRINT_PERF_INFO = 1;

    // Set recursion depth.
    RECURSION_DEPTH = 10;

    // Set default background/foreground color.
    BACKGROUND_COLOR = NewVector(0.0f, 0.0f, 0.0f);
    FOREGROUND_COLOR = NewVector(1.0f, 1.0f, 1.0f);

    // Set the shadowing mode.
    SHADOWING_MODE = SM_NO_SHADOWS;

    // Set the lighting model.
    LIGHTING_MODEL = LM_PHONG;

    // In the case of phong model, set some flags
    PHONG_SPECULAR_ENABLED = 1;
    PHONG_DIFFUSE_ENABLED = 1;

    // Other flags.
    REFLECTION_ENABLED = 1;
    REFRACTION_ENABLED = 0;

    // Set the number of pixels to be calculated as a single work unit.
    PIXELS_PER_WORK_UNIT = 100;

    // Find out the actual number of cores online right now.
    NUMBER_OF_CORES = 1;//sysconf(_SC_NPROCESSORS_ONLN);

    if (NUMBER_OF_CORES < 1)
    {
        NUMBER_OF_CORES = 1;
    }

    // Set up the camera.
    InitView(view);
    
    // Set up the scene.
    InitScene(materials, lights, objects);
}

void InitSdl(void)
{
    // Initialize the SDL library.
    if (SDL_Init(SDL_INIT_VIDEO) < 0) 
    {
        fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }

    // Clean up on exit.
    atexit(SDL_Quit);
    
    // Request a software surface inside a window.
    // IMPORTANT: Software surfaces don't require locking so they play much nicer with multi-threading.
    //            Don't switch to HWSURFACE until we are ready!
    SCREEN = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_SWSURFACE | SDL_RESIZABLE);
    
    if (SCREEN == NULL) 
    {
        fprintf(stderr, "Couldn't set video mode: %s\n", SDL_GetError());
        exit(1);
    }
}

void InitView(JsonObject * view)
{
    // Setup camera.
    CAMERA_POSITION = ParseVectorFromJsonValue(ObjectEntryByName(view, "POSITION"));
    CAMERA_LOOK_AT = ParseVectorFromJsonValue(ObjectEntryByName(view, "LOOK AT"));
    CAMERA_UP = ParseVectorFromJsonValue(ObjectEntryByName(view, "UP"));
    
    CAMERA_NEAR = FloatValue(ObjectEntryByName(view, "NEAR"));
    CAMERA_FOV_Y = (float) (DEG_TO_RAD * FloatValue(ObjectEntryByName(view, "FOV Y")));
    
    // Update the pixel plane for the camera.
    UpdatePixelPlane();
}

void UpdatePixelPlane(void)
{
    Vector cameraVector = GetNormalizedVector(SubtractVector(CAMERA_LOOK_AT, CAMERA_POSITION));
    
    pixelPlaneX = GetNormalizedVector(CrossProduct(cameraVector, CAMERA_UP));
    pixelPlaneY = GetNormalizedVector(CrossProduct(pixelPlaneX, cameraVector));
    
    float aspectRatio = (float) SCREEN_WIDTH / (float) SCREEN_HEIGHT;

    pixelPlaneHeight = 2 * CAMERA_NEAR * tan(0.5f * CAMERA_FOV_Y);
    pixelPlaneWidth = aspectRatio * pixelPlaneHeight;
    
    halfPixelWidth = 0.5f * (pixelPlaneWidth / SCREEN_WIDTH);
    halfPixelHeight = 0.5f * (pixelPlaneHeight / SCREEN_HEIGHT);
    
    pixelWidth = pixelPlaneWidth / SCREEN_WIDTH;
    pixelHeight = pixelPlaneHeight / SCREEN_HEIGHT;
    
    // Set the origin of the pixel plane at the upper left, to coincide
    // with SDL's pixel numbering.
    Vector pixelPlaneHalfWidth = GetScaledVector(pixelPlaneX, 0.5f * pixelPlaneWidth);
    Vector pixelPlaneHalfHeight = GetScaledVector(pixelPlaneY, 0.5f * pixelPlaneHeight);
    
    ScaleVector(&cameraVector, CAMERA_NEAR);
    
    pixelPlaneOrigin = AddVector(CAMERA_POSITION, cameraVector);
    pixelPlaneOrigin = AddVector(pixelPlaneOrigin, pixelPlaneHalfHeight);
    pixelPlaneOrigin = SubtractVector(pixelPlaneOrigin, pixelPlaneHalfWidth);
}

void InitScene(JsonObject * materials, JsonObject * lights, JsonArray * objects)
{
    SCENE_MATERIAL_INFOS = malloc(sizeof(ArrayList));
    SCENE_LIGHT_INFOS = malloc(sizeof(ArrayList));
    SCENE_OBJECTS = malloc(sizeof(ArrayList));
    SCENE_LIGHTS = malloc(sizeof(ArrayList));

    *SCENE_MATERIAL_INFOS = NewArrayList();
    *SCENE_LIGHT_INFOS = NewArrayList();
    *SCENE_OBJECTS = NewArrayList();
    *SCENE_LIGHTS = NewArrayList();

    MaterialInfo * last = NULL;

    // Init material infos.
    for (unsigned int i = 0; i < ObjectEntryCount(materials); i++)
    {
        JsonNameValuePair * jsonNameValue = ObjectEntryByIndex(materials, i);
        JsonObject * jsonObject = JsonObjectValue(jsonNameValue->value);
        
        MaterialInfo * materialInfo = malloc(sizeof(MaterialInfo));
        {
            materialInfo->name = jsonNameValue->name;
    
            materialInfo->diffuseColor = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "DIFFUSE COLOR"));
            materialInfo->specularColor = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "SPECULAR COLOR"));
            materialInfo->reflectivity = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "REFLECTIVITY"));
                        
            materialInfo->specularExponent = FloatValue(ObjectEntryByName(jsonObject, "SPECULAR EXPONENT"));
        }
        
        last = materialInfo;
        
        ArrayListAdd(SCENE_MATERIAL_INFOS, materialInfo);
    }

    // Init Lights.
    for (unsigned int i = 0; i < ObjectEntryCount(lights); i++)
    {
        JsonNameValuePair * jsonNameValue = ObjectEntryByIndex(lights, i);
        JsonObject * jsonObject = JsonObjectValue(jsonNameValue->value);
        
        LightInfo * lightInfo = malloc(sizeof(LightInfo));
        {
            lightInfo->name = jsonNameValue->name;
    
            lightInfo->color = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "COLOR"));
        }
        
        ArrayListAdd(SCENE_LIGHT_INFOS, lightInfo);
    }

    // Build the scene.
    for (unsigned int i = 0; i < ArrayEntryCount(objects); i++)
    {
        JsonObject * jsonObject = JsonObjectValue(ArrayEntryByIndex(objects, i));
    
        SceneObject * object = malloc(sizeof(SceneObject));
        {
            char * type = StringValue(ObjectEntryByName(jsonObject, "TYPE"));
        
            if (strcmp("Sphere", type) == 0)
            {
                Sphere * sphere = malloc(sizeof(Sphere));
            
                object->type = OBJ_TYPE_SPHERE;
                object->pointer = sphere;
                
                // Set up sphere properties.
                sphere->radius = FloatValue(ObjectEntryByName(jsonObject, "RADIUS"));
                sphere->center = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "CENTER"));
                
                sphere->materialInfo = GetMaterialInfo(jsonObject);
                sphere->lightInfo = GetLightInfo(jsonObject);
                
                // Add to the appropriate lists.
                ArrayListAdd(SCENE_OBJECTS, object);
                
                if (sphere->lightInfo != NULL)
                {
                    ArrayListAdd(SCENE_LIGHTS, object);
                }
            }
            else if (strcmp("Point", type) == 0)
            {
                Point * point = malloc(sizeof(Point));
            
                object->type = OBJ_TYPE_POINT;
                object->pointer = point;
                
                // Set up point properties.
                point->position = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "POSITION"));
                
                point->lightInfo = GetLightInfo(jsonObject);
                
                // Add to the appropriate lists.
                if (point->lightInfo != NULL)
                {
                    ArrayListAdd(SCENE_LIGHTS, object);
                }
                else
                {
                    free(point);
                    free(object);
                    continue;
                }
            }
            else if (strcmp("Triangle", type) == 0)
            {
                Triangle * triangle = malloc(sizeof(Triangle));
                
                object->type = OBJ_TYPE_TRIANGLE;
                object->pointer = triangle;
                
                // Set up triangle properties.
                triangle->v1 = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "V1"));
                triangle->v2 = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "V2"));
                triangle->v3 = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "V3"));
                triangle->normal = ParseVectorFromJsonValue(ObjectEntryByName(jsonObject, "NORMAL"));                
                
                triangle->materialInfo = GetMaterialInfo(jsonObject);
                triangle->lightInfo = GetLightInfo(jsonObject);
                
                // Add to the appropriate lists.
                ArrayListAdd(SCENE_OBJECTS, object);
                
                if (triangle->lightInfo != NULL)
                {
                    ArrayListAdd(SCENE_LIGHTS, object);
                }
            }
        }
    }
}

void GetRayForPixel(int xPixel, int yPixel, Vector * rayOrigin, Vector * rayDirection)
{
    // Note: 0,0 corresponds to the UPPER left corner.
    Vector destination = AddVector(pixelPlaneOrigin, GetScaledVector(pixelPlaneX, xPixel * pixelWidth));
           destination = AddVector(destination, GetScaledVector(pixelPlaneY, -yPixel * pixelHeight));
    
    *rayOrigin = CAMERA_POSITION;
    *rayDirection = GetNormalizedVector(SubtractVector(destination, *rayOrigin));
}

Vector ParseVectorFromJsonValue(JsonValue * jsonValue)
{
    if (jsonValue == NULL)
    {
        return NewVector(0.0f, 0.0f, 0.0f);
    }

    if (jsonValue->type != JSON_VALUE_ARRAY)
    {
        fprintf(stderr, "Cannot parse Vector from JsonArray, container JsonValue isn't a JsonArray. It has type = %d\n", jsonValue->type);
        exit(1);
    }

    JsonArray * jsonArray = (JsonArray *) jsonValue->value;

    unsigned int count = ArrayEntryCount(jsonArray);
    
    if (count != 3)
    {
        fprintf(stderr, "Cannot parse Vector from JsonArray, has count != 3. Count = %d\n", count);
        exit(1);
    }

    JsonValue * value1 = ArrayEntryByIndex(jsonArray, 0);
    JsonValue * value2 = ArrayEntryByIndex(jsonArray, 1);
    JsonValue * value3 = ArrayEntryByIndex(jsonArray, 2);
        
    if 
    (
        value1->type != JSON_VALUE_FLOAT ||
        value2->type != JSON_VALUE_FLOAT ||
        value3->type != JSON_VALUE_FLOAT
    )
    {
        fprintf(stderr, "Cannot parse Vector from JsonArray, some values aren't floats.\n");
        exit(1);
    }
        
    float * floatPtr1 = (float *) value1->value;
    float * floatPtr2 = (float *) value2->value;
    float * floatPtr3 = (float *) value3->value;        

    return NewVector(*floatPtr1, *floatPtr2, *floatPtr3);
}

MaterialInfo * GetMaterialInfo(JsonObject * jsonObject)
{
    char * materialName = StringValue(ObjectEntryByName(jsonObject, "MATERIAL"));
                    
    MaterialInfo * materialInfo = NULL;

    for (unsigned int j = 0; j < SCENE_MATERIAL_INFOS->itemCount; j++)
    {
        materialInfo = (MaterialInfo *) ArrayListGetDangerous(SCENE_MATERIAL_INFOS, j);
        
        if (strcmp(materialInfo->name, materialName) == 0)
        {
            break;
        }
    }

    if (materialInfo == NULL)
    {
        fprintf(stderr, "Could not find material with name '%s'.", materialName);
        exit(1);
    }

    return materialInfo;
}

LightInfo * GetLightInfo(JsonObject * jsonObject)
{
    JsonValue * lightEntry = ObjectEntryByName(jsonObject, "LIGHT");

    if (lightEntry != NULL)
    {
        char * lightName = StringValue(lightEntry);
    
        LightInfo * lightInfo = NULL;
        
        for (unsigned int j = 0; j < SCENE_LIGHT_INFOS->itemCount; j++)
        {
            lightInfo = (LightInfo *) ArrayListGetDangerous(SCENE_LIGHT_INFOS, j);
            
            if (strcmp(lightInfo->name, lightName) == 0)
            {
                break;
            }
        }
        
        if (lightInfo == NULL)
        {
            fprintf(stderr, "Could not find light with name '%s'.", lightName);
            exit(1);
        }
        
        return lightInfo;
    }
    
    return NULL;
}
