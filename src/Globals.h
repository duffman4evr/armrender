#ifndef _ARMRENDER_GLOBALS_H
#define _ARMRENDER_GLOBALS_H

#include <SDL.h>

#include "ArrayList.h"
#include "Types.h"

// SDL drawing surface.
extern SDL_Surface * SCREEN;

// Screen resolution.
extern unsigned int SCREEN_WIDTH;
extern unsigned int SCREEN_HEIGHT;

// Camera Info
extern Vector CAMERA_POSITION;
extern Vector CAMERA_LOOK_AT;
extern Vector CAMERA_UP;

// Shadowing mode.
extern unsigned int SHADOWING_MODE;

// Shadowing mode enum.
#define SM_NO_SHADOWS   0
#define SM_HARD_SHADOWS 1
#define SM_SOFT_SHADOWS 2

// Lighting model.
extern unsigned int LIGHTING_MODEL;

// Lighting model enum.
#define LM_NONE         0
#define LM_PHONG        1
#define LM_BLINN_PHONG  2

// Phong model flags.
extern unsigned int PHONG_SPECULAR_ENABLED;
extern unsigned int PHONG_DIFFUSE_ENABLED;

// Other flags.
extern unsigned int REFLECTION_ENABLED;
extern unsigned int REFRACTION_ENABLED;

// Number of cores on this system.
extern unsigned int NUMBER_OF_CORES;

// How many pixels should be calculated as a single work item.
extern unsigned int PIXELS_PER_WORK_UNIT;

// How far can we recurse doing recursive raytracing.
extern unsigned int RECURSION_DEPTH;

// Default Background/Foreground color.
extern Vector BACKGROUND_COLOR;
extern Vector FOREGROUND_COLOR;

// Scene objects.
extern ArrayList * SCENE_OBJECTS;
extern ArrayList * SCENE_LIGHTS;

// Logging.
extern unsigned int PRINT_PERF_INFO;

// Init routines.
void InitGlobals(void);

// Helpers.
void GetRayForPixel(int xPixel, int yPixel, Vector * rayOrigin, Vector * rayDirection);
void UpdatePixelPlane(void);

#endif
