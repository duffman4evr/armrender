#include <stdlib.h>
#include <string.h>

#include "JsonParser.h"

// Defines.

// Log level: 
//  2 -> Objects and Primitives
//  0 -> Errors Only
#define LOG_LEVEL 0

//-----------------------------
// Private functions.
//-----------------------------

// Json object parsers.
JsonValue *         ParseJsonValue(FILE * file);
JsonObject *        ParseJsonObject(FILE * file);
JsonArray *         ParseJsonArray(FILE * file);
JsonNameValuePair * ParseJsonNameValuePair(FILE * file);
char *              ParseJsonString(FILE * file);
float *             ParseJsonFloat(FILE * file);
unsigned int *      ParseJsonBoolean(FILE * file);
void *              ParseJsonNull(FILE * file);

// Helper functions
unsigned int IsWhitespace(char character);
unsigned int IsDigit(char character);

char SkipWhitespace(FILE * file);

// Private variables.
char lastRead;

//-----------------------------
// Public functions.
//-----------------------------

JsonValue * ParseJson(FILE * file)
{
    lastRead = SkipWhitespace(file);
    return ParseJsonValue(file);
}

JsonValue * ObjectEntryByName(JsonObject * jsonObject, char * name)
{
    for (unsigned int i = 0; i < jsonObject->arrayList.itemCount; i++)
    {
        JsonNameValuePair * nameValuePair = (JsonNameValuePair *) ArrayListGetDangerous(&(jsonObject->arrayList), i);
        
        if (strcmp(nameValuePair->name, name) == 0)
        {
            return nameValuePair->value;
        }
    }  
    
    return NULL;
}

JsonNameValuePair * ObjectEntryByIndex(JsonObject * jsonObject, int index)
{
    return (JsonNameValuePair *) ArrayListGet(&(jsonObject->arrayList), index);
}

unsigned int ObjectEntryCount(JsonObject * jsonObject)
{
    return jsonObject->arrayList.itemCount;
}

JsonValue * ArrayEntryByIndex(JsonArray * jsonArray, int index)
{
    return (JsonValue *) ArrayListGet(&(jsonArray->arrayList), index);
}

unsigned int ArrayEntryCount(JsonArray * jsonArray)
{
    return jsonArray->arrayList.itemCount;
}

JsonObject * JsonObjectValue(JsonValue * jsonValue)
{
    if (jsonValue == NULL)
    {
        return NULL;
    }

    if (jsonValue->type != JSON_VALUE_OBJECT)
    {
        fprintf(stderr, "Cannot get JsonObject value of non-JsonObject type.\n");
        exit(1);
    }
    
    return (JsonObject *) jsonValue->value;
}

int IntegerValue(JsonValue * jsonValue)
{
    if (jsonValue == NULL)
    {
        return 0;
    }

    if (jsonValue->type != JSON_VALUE_FLOAT)
    {
        fprintf(stderr, "Cannot get integer value of non-float type.\n");
        exit(1);
    }
    
    float * result = (float *) jsonValue->value;
    
    return (int) (*result);
}

float FloatValue(JsonValue * jsonValue)
{
    if (jsonValue == NULL)
    {
        return 0.0f;
    }

    if (jsonValue->type != JSON_VALUE_FLOAT)
    {
        fprintf(stderr, "Cannot get float value of non-float type.\n");
        exit(1);
    }
    
    float * result = (float *) jsonValue->value;
    
    return *result;
}

char * StringValue(JsonValue * jsonValue)
{
    if (jsonValue == NULL)
    {
        return NULL;
    }

    if (jsonValue->type != JSON_VALUE_STRING)
    {
        fprintf(stderr, "Cannot get string value of non-string type.\n");
        exit(1);
    }
    
    char * result = (char *) jsonValue->value;
    
    return result;
}

//-----------------------------
// Private impls.
//-----------------------------

// This is expected to be called with file pointer reading at the character just after the first one
JsonValue * ParseJsonValue(FILE * file)
{
    JsonValue * jsonValue = malloc(sizeof(JsonValue));

    switch (lastRead)
    {
        case '{':
            jsonValue->type = JSON_VALUE_OBJECT;
            jsonValue->value = ParseJsonObject(file);
            break;
        case '[':
            jsonValue->type = JSON_VALUE_ARRAY;
            jsonValue->value = ParseJsonArray(file);
            break;
        case '"':
            jsonValue->type = JSON_VALUE_STRING;
            jsonValue->value = ParseJsonString(file);
            break;
        case '-':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            jsonValue->type = JSON_VALUE_FLOAT;
            jsonValue->value = ParseJsonFloat(file);                                                                                             
            break;
        case 't':
        case 'f':
            jsonValue->type = JSON_VALUE_BOOLEAN;
            jsonValue->value = ParseJsonBoolean(file);
            break;
        case 'n':
            jsonValue->type = JSON_VALUE_NULL;
            jsonValue->value = ParseJsonNull(file);
            break;
        default:
            fprintf(stderr, "Unrecognized start of JsonValue: %c\n", lastRead);
            exit(1);
    }
    
    return jsonValue;
}

// This is expected to be called with file pointer reading at the character just after the '{'
JsonObject * ParseJsonObject(FILE * file)
{
    JsonObject * jsonObject = malloc(sizeof(JsonObject));
    {
        jsonObject->arrayList = NewArrayList();
    }

    lastRead = SkipWhitespace(file);
    
    while (1)
    {
        if (lastRead != '"')
        {
            fprintf(stderr, "Did not expect '%c' as start of JsonNameValuePair.", lastRead);
            exit(1);
        }
        
        ArrayListAdd(&(jsonObject->arrayList), ParseJsonNameValuePair(file));
        
        lastRead = SkipWhitespace(file);
        
        if (lastRead == '}')
        {
            return jsonObject;
        }
        else if (lastRead == ',')
        {
            lastRead = SkipWhitespace(file);
        }
        else 
        {
            fprintf(stderr, "Did not expect '%c' as end of THINGY.", lastRead);
            exit(1);
        }
    }
    
    return jsonObject;
}

// This is expected to be called with file pointer reading at the character just after the '['
JsonArray * ParseJsonArray(FILE * file)
{
    JsonArray * jsonArray = malloc(sizeof(JsonArray));
    {
        jsonArray->arrayList = NewArrayList();
    }

    lastRead = SkipWhitespace(file);
    
    while (1)
    {   
        ArrayListAdd(&(jsonArray->arrayList), ParseJsonValue(file));
        
        lastRead = SkipWhitespace(file);
        
        if (lastRead == ']')
        {
            return jsonArray;
        }
        else if (lastRead == ',')
        {
            lastRead = SkipWhitespace(file);
        }
        else 
        {
            fprintf(stderr, "Did not expect '%c' as end of THINGY.", lastRead);
            exit(1);
        }
    }
    
    return jsonArray;
}

// This is expected to be called with file pointer reading at the character just after the '"'
JsonNameValuePair * ParseJsonNameValuePair(FILE * file)
{
    JsonNameValuePair * nameValuePair = malloc(sizeof(JsonNameValuePair));
    
    nameValuePair->name = ParseJsonString(file);
    
    lastRead = SkipWhitespace(file);
    
    if (lastRead != ':')
    {
        fprintf(stderr, "Missing ':' in JsonNameValuePair. Instead found '%c'", lastRead);
        exit(1);
    }
    
    lastRead = SkipWhitespace(file);
    
    nameValuePair->value = ParseJsonValue(file);
    
    return nameValuePair;
}

char * ParseJsonString(FILE * file)
{
    ArrayList arrayList = NewArrayList();
    
    // Add the string into the ArrayList.
    lastRead = fgetc(file);
    
    while (lastRead != '"')
    {
        if (lastRead == EOF)
        {
            fprintf(stderr, "Did not expect EOF in middle of a string.", lastRead);
            exit(1);
        }
        
        ArrayListAdd(&arrayList, (void *) lastRead);
        
        lastRead = fgetc(file);
    }
    
    // Add the null termination.
    ArrayListAdd(&arrayList, (void *) NULL);
    
    // Copy it into a C-style string.
    char * string = malloc(sizeof(char) * arrayList.itemCount);
    
    for (unsigned int i = 0; i < arrayList.itemCount; i++)
    {
         string[i] = (char) ArrayListGetDangerous(&arrayList, i);
    }
    
    // Dispose the ArrayList. 
    DisposeArrayList(&arrayList);
    
    if (LOG_LEVEL == 2)
    {
        fprintf(stdout, "Parsed in this string: '%s'\n", string);
    }
    
    return string;
}

// We don't do full validation here. Maybe add it later if bored.
float * ParseJsonFloat(FILE * file)
{
    ArrayList arrayList = NewArrayList();
    
    unsigned int dotSeen = 0;
    
    // Create a C string that stores the float.
    while (IsDigit(lastRead) == 1 || lastRead == '.' || lastRead == '-' || lastRead == EOF)
    {
        if (lastRead == EOF)
        {
            fprintf(stderr, "Did not expect EOF in middle of a float.", lastRead);
            exit(1);
        }
        else if (lastRead == '.')
        {
            if (dotSeen == 1)
            {
                fprintf(stderr, "Did not expect more than one '.' in middle of a float.", lastRead);
                exit(1);
            }
            
            dotSeen = 1;
        }
    
        ArrayListAdd(&arrayList, (void *) lastRead);
        
        lastRead = fgetc(file);
        
        if (lastRead == '-')
        {
            fprintf(stderr, "Did not expect '-' in middle of a float.", lastRead);
            exit(1);
        }
    }
    
    ungetc((int) lastRead, file);
    
    // Add the null termination.
    ArrayListAdd(&arrayList, (void *) NULL);
    
    // Copy it into a C-style string.
    char string[arrayList.itemCount];
    
    for (unsigned int i = 0; i < arrayList.itemCount; i++)
    {
        string[i] = (char) ArrayListGetDangerous(&arrayList, i);
    }
    
    // Dispose the ArrayList.
    DisposeArrayList(&arrayList);
    
    double result = atof(string);
    
    float * floatResult = malloc(sizeof(float));
    
    *floatResult = (float) result;
    
    if (LOG_LEVEL == 2)
    {
        fprintf(stdout, "Parsed in this float: '%f'\n", *floatResult);
    }
    
    return floatResult;
}

unsigned int * ParseJsonBoolean(FILE * file)
{
    unsigned int * result = malloc(sizeof(unsigned int));

    if (lastRead == 't')
    {
        char r = fgetc(file);
        char u = fgetc(file);
        char e = fgetc(file);
        
        if 
        (
            r != 'r' ||
            u != 'u' || 
            e != 'e'
        ) 
        {
            fprintf(stderr, "Wierd looking 'true': %c%c%c%c", lastRead, r, u, e);
            exit(1);
        }
        
        *result = 1;

        if (LOG_LEVEL == 2)
        {
            fprintf(stdout, "Parsed in 'true'\n");
        }
        
        return result;
    }
    else if (lastRead == 'f')
    {
        char a = fgetc(file);
        char l = fgetc(file);
        char s = fgetc(file);
        char e = fgetc(file);
        
        if 
        (
            a != 'a' ||
            l != 'l' || 
            s != 's' || 
            e != 'e'
        ) 
        {
            fprintf(stderr, "Wierd looking 'false': %c%c%c%c%c", lastRead, a, l, s, e);
            exit(1);
        }
        
        *result = 0;
        
        if (LOG_LEVEL == 2)
        {
            fprintf(stdout, "Parsed in 'false'\n");
        }
        
        return result;
    }
    else 
    {
        fprintf(stderr, "Invalid call to ParseJsonBoolean with %c", lastRead);
        exit(1);
    }
}

void * ParseJsonNull(FILE * file)
{
    char u = fgetc(file);
    char l1 = fgetc(file);
    char l2 = fgetc(file);
    
    if 
    (
        u  != 'u' ||
        l1 != 'l' || 
        l2 != 'l'
    ) 
    {
        fprintf(stderr, "Wierd looking 'null': %c%c%c", 'n', u, l1, l2);
        exit(1);
    }
    
    if (LOG_LEVEL == 2)
    {
        fprintf(stdout, "Parsed in 'null'\n");
    }
    
    return NULL;
}

unsigned int IsWhitespace(char character)
{
    if ((character >= 0) && (character < 33))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

unsigned int IsDigit(char character)
{
    if  ((character >= 47) && (character < 58))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

char SkipWhitespace(FILE * file)
{
    lastRead = fgetc(file);
    
    while (IsWhitespace(lastRead) == 1)
    {
        lastRead = fgetc(file);
    }
    
    return lastRead;
}
