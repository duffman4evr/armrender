#ifndef _ARMRENDER_JSON_PARSER_H_
#define _ARMRENDER_JSON_PARSER_H_

#include <stdio.h>

#include "ArrayList.h"

#define JSON_VALUE_OBJECT   1
#define JSON_VALUE_STRING   2
#define JSON_VALUE_FLOAT    3
#define JSON_VALUE_ARRAY    4
#define JSON_VALUE_BOOLEAN  5
#define JSON_VALUE_NULL     6

#define NODE_TYPE_VALUE             1
#define NODE_TYPE_NAME_VALUE_PAIR   2

// Structs.
typedef struct 
{
    unsigned int type;
    void * value;
} 
JsonValue;

typedef struct 
{
    char * name;
    JsonValue * value;
} 
JsonNameValuePair;

typedef struct 
{
    ArrayList arrayList;
} 
JsonObject;

typedef struct 
{
    ArrayList arrayList;
} 
JsonArray;

// Functions.
JsonValue * ParseJson(FILE * file);

JsonValue * ObjectEntryByName(JsonObject * jsonObject, char * name);
JsonNameValuePair * ObjectEntryByIndex(JsonObject * jsonArray, int index);
unsigned int ObjectEntryCount(JsonObject * jsonArray);

JsonValue * ArrayEntryByIndex(JsonArray * jsonArray, int index);
unsigned int ArrayEntryCount(JsonArray * jsonArray);

JsonObject * JsonObjectValue(JsonValue * jsonValue);
int IntegerValue(JsonValue * jsonValue);
float FloatValue(JsonValue * jsonValue);
char * StringValue(JsonValue * jsonValue);


#endif
