#include <stdlib.h>
#include <pthread.h>
#include <SDL.h>

#include "Util.h"

int main(int argc, char * argv[])
{
    // Initialize our global variables.
    InitGlobals();
    
    // Create a blocking queue for producer/consumer threads to use.
    BlockingQueue workAssignQueue = NewBlockingQueue(200);
    BlockingQueue workCompleteQueue = NewBlockingQueue(50000);
    
    QueuePair queuePair = NewQueuePair(&workAssignQueue, &workCompleteQueue);
    
    pthread_attr_t threadAttributes;
    pthread_attr_init(&threadAttributes);
    pthread_attr_setdetachstate(&threadAttributes, PTHREAD_CREATE_JOINABLE);
    
    // Create and start a producer thread that will create jobs for X consumer threads.
    pthread_t producerThread;
    
    int rc = pthread_create(&producerThread, &threadAttributes, ProduceWork, (void *) &queuePair);
          
    if (rc)
    {
        fprintf(stderr, "Could not create producer thread. Return code from pthread_create() is %d\n", rc);
        exit(1);
    }

    // Create and start a X consumer threads.
    unsigned int numberOfThreads = NUMBER_OF_CORES;
    
    pthread_t consumerThreads[numberOfThreads];
    
    for (unsigned int i = 0; i < numberOfThreads; i++)
    {
        int rc = pthread_create(&consumerThreads[i], &threadAttributes, ConsumeWork, (void *) &queuePair);
              
        if (rc)
        {
            fprintf(stderr, "Could not create consumer thread. Return code from pthread_create() is %d\n", rc);
            exit(1);
        }
    }
    
    // Enter an event loop, waiting for the user to close the window.
    SDL_Event event; 
    
    while (1)
    {
        if (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT: 
                    // User closed the window.
                    
                    // Set blocking queues to kill their respective threads.
                    AlertBlockingQueueForHalt(&workAssignQueue);
                    AlertBlockingQueueForHalt(&workCompleteQueue);
                    
                    // Wait for the threads to actually stop.
                    pthread_join(producerThread, NULL);
                    
                    for (unsigned int i = 0; i < numberOfThreads; i++) 
                    {
                        pthread_join(consumerThreads[i], NULL);
                    }
                    
                    // Clean up remaining loose ends.
                    DisposeBlockingQueue(&workAssignQueue);
                    DisposeBlockingQueue(&workCompleteQueue);
                    
                    pthread_attr_destroy(&threadAttributes);
                    
                    SDL_FreeSurface(SCREEN);
                    
                    fprintf(stdout, "User has closed the window.\n");
                    
                    exit(0);
            }
        }
    }    
    
    fprintf(stderr, "Should never make it here.");
    return 1;
}
