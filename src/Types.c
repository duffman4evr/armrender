#include "Types.h"

SceneObject NewSceneObject(unsigned int type, void * pointer)
{
    SceneObject object;
    
    object.type = type;
    object.pointer = pointer;
    
    return object;
}
