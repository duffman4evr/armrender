#ifndef _ARMRENDER_TYPES_H
#define _ARMRENDER_TYPES_H

#define OBJ_TYPE_POINT     1
#define OBJ_TYPE_SPHERE    2
#define OBJ_TYPE_TRIANGLE  3

#include "Vector.h"
#include "BlockingQueue.h"

// Structs
typedef struct 
{
    unsigned int type;
    void * pointer;
} 
SceneObject;

typedef struct 
{
    char * name;
    
    Vector color;
} 
LightInfo;

typedef struct 
{
    char * name;
    
    Vector diffuseColor;
    Vector specularColor;
    Vector reflectivity;
    
    int specularExponent;
} 
MaterialInfo;

typedef struct 
{
    Vector position;
    LightInfo * lightInfo;
} 
Point;

typedef struct 
{
    Vector center;
    float radius;
    MaterialInfo * materialInfo;
    LightInfo * lightInfo;
} 
Sphere;

typedef struct 
{
    Vector v1;
    Vector v2;
    Vector v3;
    Vector normal;
    MaterialInfo * materialInfo;
    LightInfo * lightInfo;
} 
Triangle;

typedef struct 
{
    unsigned int xStart;
    unsigned int xEnd;
    unsigned int yStart;
    unsigned int yEnd;
} 
RaytraceWorkItem;

// Constructors
SceneObject NewSceneObject(unsigned int type, void * pointer);

#endif
