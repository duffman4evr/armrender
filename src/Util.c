#include <pthread.h>
#include <math.h>
#include <float.h>

#include "Util.h"

// Private defines.
#define FLOAT_EPSILON 0.000001

// Private functions
int IsZeroVector(Vector vector);

// Function implementations.
Vector GetColorForRay(Vector rayOrigin, Vector rayDirection, int recursionDepth)
{
    if (recursionDepth > RECURSION_DEPTH)
    {
        // We have recursed too deeply. Return background color.
        return BACKGROUND_COLOR;
    }

    // First, find any intersections with objects.
    SceneObject * intersectedObject = 0;
    Vector intersection;
    float intersectionTval;

    GetClosestIntersection(rayOrigin, rayDirection, &intersectedObject, &intersection, &intersectionTval);

    if (intersectedObject == 0)
    {
        // If we didn't find an intersection, return background color.
        return BACKGROUND_COLOR;
    }

    //--------------------------
    // Do local lighting.
    //--------------------------
    
    Vector color = NewVector(0.0f, 0.0f, 0.0f);

    Vector reversedRayDirection = GetScaledVector(rayDirection, -1.0f);
    MaterialInfo * materialInfo;
    Vector normal;

    if (intersectedObject->type == OBJ_TYPE_SPHERE)
    {
        Sphere * sphere = (Sphere *) intersectedObject->pointer;

        materialInfo = sphere->materialInfo;
        
        normal = SubtractVector(intersection, sphere->center);
        NormalizeVector(&normal);
    }
    else if (intersectedObject->type == OBJ_TYPE_TRIANGLE)
    {
        Triangle * triangle = (Triangle *) intersectedObject->pointer;

        materialInfo = triangle->materialInfo;

        // We trust that the scene data gives us normalized normals.
        normal = triangle->normal;
    }

    if (LIGHTING_MODEL == LM_NONE)
    {
        return FOREGROUND_COLOR;
    }
    
    if (LIGHTING_MODEL == LM_PHONG)
    {
        AddPhongColor(&color, rayDirection, &reversedRayDirection, intersectedObject, intersection, materialInfo, normal);
    }
    
    //--------------------------
    // Do reflections.
    //--------------------------
    
    Vector reflectivity = materialInfo->reflectivity;
    
    if ((REFLECTION_ENABLED == 1) && (IsZeroVector(reflectivity) == 0))
    {
        Vector reflection = CalculateReflectedVector(reversedRayDirection, normal);
        Vector reflectionOrig = AddVector(intersection, GetScaledVector(reflection, 0.01f));
    
        Vector reflectedColor = GetColorForRay(reflectionOrig, reflection, recursionDepth + 1);
    
        color.x += reflectivity.x * reflectedColor.x;
        color.y += reflectivity.y * reflectedColor.y;
        color.z += reflectivity.z * reflectedColor.z;
    }
    
    // Clamp the color down to 1.0
    color.x = (color.x > 1.0f) ? 1.0f : color.x;
    color.y = (color.y > 1.0f) ? 1.0f : color.y;
    color.z = (color.z > 1.0f) ? 1.0f : color.z;
    
    return color;
}

void AddPhongColor(Vector * color, Vector rayDirection, Vector * reversedRayDirection, SceneObject * intersectedObject, Vector intersection, MaterialInfo * materialInfo, Vector normal)
{
    // For each light...
    for (unsigned int i = 0; i < SCENE_LIGHTS->itemCount; i++)
    {
        SceneObject * object = ArrayListGet(SCENE_LIGHTS, i);

        // For now we just use point lights.
        Point * light = (Point *) object->pointer;

        // Define the ray from intersection point towards the light.
        // Ray origin is the intersection point, and ray destination is the light.
        Vector lightRay = GetNormalizedVector(SubtractVector(light->position, intersection));

        if (SHADOWING_MODE == SM_HARD_SHADOWS)
        {
            // Find any objects that may be casting a shadow on this one.
            SceneObject * shadowCastingObject;
            Vector shadowIntersection;
            float shadowIntersectionTval;
            
            // We have to add a tiny amount to the intersection point in the direction of the
            // lightRay to avoid intersecting with the point that we are starting from!
            Vector shadowRayOrigin = AddVector(intersection, GetScaledVector(lightRay, 0.01f));
            
            GetClosestIntersection(shadowRayOrigin, lightRay, &shadowCastingObject, &shadowIntersection, &shadowIntersectionTval);

            // If we got an intersection back, and its closer to us than the light itself, then we are in shadow.
            if (shadowCastingObject != 0 && VectorMagnitude(SubtractVector(light->position, intersection)) > shadowIntersectionTval)
            {
                continue;
            }
        }

        // Add in diffuse color.
        Vector diffuseMaterial = materialInfo->diffuseColor;
        
        if ((PHONG_DIFFUSE_ENABLED == 1) && (IsZeroVector(diffuseMaterial) == 0))
        {
            float diffuseTerm = PositiveDotProduct(normal, lightRay);
            
            if (FloatEqual(diffuseTerm, 0.0f) == 0)
            {
                color->x += light->lightInfo->color.x * diffuseMaterial.x * diffuseTerm;
                color->y += light->lightInfo->color.y * diffuseMaterial.y * diffuseTerm;
                color->z += light->lightInfo->color.z * diffuseMaterial.z * diffuseTerm;
            }
        }

        // Add in specular color.
        Vector specularMaterial = materialInfo->specularColor;
        
        if ((PHONG_SPECULAR_ENABLED == 1) && (IsZeroVector(specularMaterial) == 0))
        {
            Vector reflection = CalculateReflectedVector(lightRay, normal);
            
            float specularBase = PositiveDotProduct(reflection, *reversedRayDirection);
            
            if (FloatEqual(specularBase, 0.0f) == 0)
            {
                int specularExponent = materialInfo->specularExponent;
            
                float specularTerm = pow(specularBase, specularExponent);
                
                color->x += light->lightInfo->color.x * specularMaterial.x * specularTerm;
                color->y += light->lightInfo->color.y * specularMaterial.y * specularTerm;
                color->z += light->lightInfo->color.z * specularMaterial.z * specularTerm;
            }
        }
    }
}

Vector CalculateReflectedVector(Vector vector, Vector normal)
{
    float dotProduct = DotProduct(normal, vector);
    
    ScaleVector(&vector, -1.0f);
    ScaleVector(&normal, 2 * dotProduct);

    return GetNormalizedVector(AddVector(vector, normal));
}

void GetClosestIntersection(Vector rayOrigin, Vector rayDirection, SceneObject ** intersectedObject, Vector * intersection, float * intersectionTval)
{
    // Set passed intersection val to the maximum float value.
    *intersectedObject = 0;
    *intersectionTval = FLT_MAX;

    for (unsigned int i = 0; i < SCENE_OBJECTS->itemCount; i++)
    {
        SceneObject * object = ArrayListGetDangerous(SCENE_OBJECTS, i);
        
        if (object->type == OBJ_TYPE_SPHERE)
        {
            Sphere * sphere = object->pointer;
            
            float intersect1;
            float intersect2;
            
            int numIntersections = GetSphereIntersection(rayOrigin, rayDirection, sphere->center, sphere->radius, &intersect1, &intersect2);

            if (numIntersections == 0)
            {
                continue;
            }
            else if (numIntersections == 1)
            {
                UpdateIntersection(intersectedObject, intersectionTval, object, intersect1);
            }
            else if (numIntersections == 2)
            {
                UpdateIntersection(intersectedObject, intersectionTval, object, intersect1);
                UpdateIntersection(intersectedObject, intersectionTval, object, intersect2);
            }
            else
            {
                fprintf(stderr, "GetSphereIntersection returned %d! Aborting...\n", numIntersections);
            }
        }
        else if (object->type == OBJ_TYPE_TRIANGLE)
        {
            Triangle * triangle = object->pointer;
            
            float intersect;
            
            int numIntersections = GetTriangleIntersection(rayOrigin, rayDirection, triangle->v1, triangle->v2, triangle->v3, &intersect);

            if (numIntersections == 0)
            {
                continue;
            }
            else if (numIntersections == 1)
            {
                UpdateIntersection(intersectedObject, intersectionTval, object, intersect);
            }
            else
            {
                fprintf(stderr, "GetTriangleIntersection returned %d! Aborting...\n", numIntersections);
            }
        }
    }
    
    if (*intersectedObject != 0)
    {
        ScaleVector(&rayDirection, *intersectionTval);
        *intersection = AddVector(rayOrigin, rayDirection);
    }
}

void UpdateIntersection(SceneObject ** storedObject, float * storedTval, SceneObject * intersectedObject, float intersectionTval)
{
    if (intersectionTval < 0.0f)
    {
        return;
    }

    if (intersectionTval < *storedTval)
    {
        *storedTval = intersectionTval;
        *storedObject = intersectedObject;
    }
}

// Gets the intersection of a ray and a triangle. 
// Returns:
//   0 if no intersection found.
//   1 if an intersection is found. The value for t can be found in argument 'intersect'.
unsigned int GetTriangleIntersection(Vector p0, Vector pd, Vector v0, Vector v1, Vector v2, float * intersect)
{
    Vector e1 = SubtractVector(v1, v0);
    Vector e2 = SubtractVector(v2, v0);
    Vector p  = CrossProduct(pd, e2);
            
    float a = DotProduct(e1, p);

    if (FloatEqual(a, 0.0f))
        return 0;

    float f = 1.0f / a;

    Vector s = SubtractVector(p0, v0);

    float u = f * DotProduct(s, p);
 
    if ((u < 0.0f) || (u > 1.0f))
        return 0;

    Vector q = CrossProduct(s, e1);

    float v = f * DotProduct(pd, q);

    if ((v < 0.0f) || (u + v) > 1.0f + FLOAT_EPSILON)
        return 0;

    *intersect = f * DotProduct(e2, q);
    
    return 1;
}

// Gets the intersection(s) of a ray and a sphere. 
// Returns:
//   0 if no intersection found.
//   1 if just one intersection is found. The value for t can be found in argument 'intersect1'.
//   2 if two intersections are found. The values for t can be found in arguments 'intersect1' and 'intersect2'.
unsigned int GetSphereIntersection(Vector p0, Vector pd, Vector center, float radius, float * intersect1, float * intersect2)
{
    // Note: a is always 1 here because we have a unit vector for direction.

    // Compute b^2 - 4c
    // If it is negative, 0 solutions.
    // If it is 0, 1 solution.
    // If it is positive, 2 solutions.
    float b = 2 * (
                    (pd.x * (p0.x - center.x)) + 
                    (pd.y * (p0.y - center.y)) + 
                    (pd.z * (p0.z - center.z))
                  );

    float c1 = p0.x - center.x;
    float c2 = p0.y - center.y;
    float c3 = p0.z - center.z;

    float c = (c1 * c1) + (c2 * c2) + (c3 * c3) - (radius * radius);

    float b2m4c = (b * b) - (4 * c);

    if (b2m4c < -0.00001)
    {
        return 0;
    }
    else if (FloatEqual(b2m4c, 0.0f))
    {
        *intersect1 = 0.5f * -b;
        return 1;
    }
    else
    {
        *intersect1 = 0.5f * (-b + sqrt(b2m4c));
        *intersect2 = 0.5f * (-b - sqrt(b2m4c));
        return 2;
    }
}

Vector GetColorForPixel(Uint32 xPixel, Uint32 yPixel)
{
    Vector rayOrigin;
    Vector rayDirection;
    
    // Use utility function to calculate ray origin/direction given pixel.
    GetRayForPixel(xPixel, yPixel, &rayOrigin, &rayDirection);
    
    // Get the color for that ray.
    return GetColorForRay(rayOrigin, rayDirection, 0);
}

void * ProduceWork(void * queues)
{
    QueuePair * queuePair = (QueuePair *) queues;

    BlockingQueue * workAssignQueue = queuePair->queueA;
    BlockingQueue * workCompleteQueue = queuePair->queueB;

    struct timeval startFrame;
    struct timeval endFrame;

    float theta = 0.0f;

    // Outer loop - render a frame each iteration.
    while (1)
    {
        if (PRINT_PERF_INFO)
        {
            gettimeofday(&startFrame, NULL);
        }
    
        unsigned int xStart = 0;
        unsigned int yStart = 0;
        
        unsigned int xEnd = PIXELS_PER_WORK_UNIT % SCREEN_WIDTH;
        unsigned int yEnd = PIXELS_PER_WORK_UNIT / SCREEN_WIDTH;
        
        unsigned int assignedWorkItemCount = 0;
        
        // Inner loop - dispatch a work item each iteration.
        while (1)
        {
            // Create the work item.
            RaytraceWorkItem * workItem = malloc(sizeof(RaytraceWorkItem));
            
            // Set the work item parameters.
            workItem->xStart = xStart;
            workItem->xEnd = xEnd;
            workItem->yStart = yStart;
            workItem->yEnd = yEnd;
            
            // Add the work item to the queue.
            BlockingQueueAdd(workAssignQueue, (void *) workItem);
            
            assignedWorkItemCount++;
            
            if (yEnd >= SCREEN_HEIGHT)
            {
                // We just created the last work item.
                break;
            }
            
            xStart = xEnd;
            yStart = yEnd;
            
            xEnd = (xStart + PIXELS_PER_WORK_UNIT) % SCREEN_WIDTH;
            yEnd = yStart + ((xStart + PIXELS_PER_WORK_UNIT) / SCREEN_WIDTH);
        }
        
        // Wait until all of the work is complete.
        for (unsigned int i = 0; i < assignedWorkItemCount; i++)
        {
            // Also free the work items.
            free(BlockingQueueRemove(workCompleteQueue));
        }
        
        // Update the screen.
        SDL_UpdateRect(SCREEN, 0, 0, 0, 0);
        
        if (PRINT_PERF_INFO)
        {
            gettimeofday(&endFrame, NULL);
            PrintElapsedTime(startFrame, endFrame);
        }
        
        // Move some shit around.
        theta += .05;
        CAMERA_POSITION.x = 2 * sin(theta);
        CAMERA_POSITION.z = 2 * cos(theta);
        UpdatePixelPlane();
    }
    
    pthread_exit(NULL);
}

void * ConsumeWork(void * queues)
{
    QueuePair * queuePair = (QueuePair *) queues;

    BlockingQueue * workAssignQueue = queuePair->queueA;
    BlockingQueue * workCompleteQueue = queuePair->queueB;
    
    // Outer loop - Grab a work item each iteration.
    while (1)
    {
        RaytraceWorkItem * workItem = (RaytraceWorkItem *) BlockingQueueRemove(workAssignQueue);
        
        unsigned int xPixel = workItem->xStart;
        unsigned int yPixel = workItem->yStart;
        
        // Inner loop - calculate a pixel each iteration.
        while (1)
        {
            // Get the color for the given pixel here.
            Vector color = GetColorForPixel(xPixel, yPixel);

            if (color.x > 0.001f)
            {
                int j = 10;
            }

            // Convert it to RGB [0-255]. We use simple truncating 
            // conversion here. We can do something else later.
            Uint8 red   = (Uint8) (color.x * 255);
            Uint8 green = (Uint8) (color.y * 255);
            Uint8 blue  = (Uint8) (color.z * 255);

            // Draw the pixel.
            DrawPixel(SCREEN, red, green, blue, xPixel, yPixel);
            
            xPixel++;

            if (xPixel >= SCREEN_WIDTH)
            {
                xPixel = 0;
                
                yPixel++;
                
                if (yPixel >= SCREEN_HEIGHT)
                {
                    break;
                }
            }
            
            if ((yPixel >= workItem->yEnd) && (xPixel >= workItem->xEnd))
            {
                break;
            }
        }
        
        // Pixel calculation complete - put this work item into the 'done' queue.
        BlockingQueueAdd(workCompleteQueue, (void *) workItem);
    }
   
    pthread_exit(NULL);
}

void PrintElapsedTime(struct timeval start, struct timeval end)
{
    long mtime;
    long seconds;
    long useconds;    

    seconds  = end.tv_sec  - start.tv_sec;
    useconds = end.tv_usec - start.tv_usec;

    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    
    fprintf(stderr, "Render time %ldms\n", mtime);
}

int FloatEqual(float f1, float f2)
{
    if ((f1 < (f2 + FLOAT_EPSILON)) && (f1 > (f2 - FLOAT_EPSILON)))
        return 1;
    else
        return 0;
}

int IsZeroVector(Vector vector)
{
    if 
    (
        FloatEqual(vector.x, 0.0f) == 1 &&
        FloatEqual(vector.y, 0.0f) == 1 &&
        FloatEqual(vector.z, 0.0f) == 1
    )
    {
        return 1;
    }            
    else
    {
        return 0;
    } 
}

void DrawPixel(SDL_Surface * screen, Uint8 r, Uint8 g, Uint8 b, Uint32 x, Uint32 y)
{
    Uint32 color = SDL_MapRGB(screen->format, r, g, b);

    if (SDL_MUSTLOCK(screen)) 
    {
        if (SDL_LockSurface(screen) < 0) 
        {
            return;
        }
    }
    
    switch (screen->format->BytesPerPixel) 
    {
        case 1: 
        { 
            // Assuming 8-bpp 
            Uint8 * bufp;
            bufp = (Uint8 *) screen->pixels + y * screen->pitch + x;
            *bufp = color;
        }
        break;

        case 2: 
        { 
            // Probably 15-bpp or 16-bpp
            Uint16 * bufp;
            bufp = (Uint16 *) screen->pixels + y*screen->pitch / 2 + x;
            *bufp = color;
        }
        break;

        case 3: 
        { 
            // Slow 24-bpp mode, usually not used
            Uint8 * bufp;

            bufp = (Uint8 *) screen->pixels + y * screen->pitch + x;
            
            *(bufp+screen->format->Rshift / 8) = r;
            *(bufp+screen->format->Gshift / 8) = g;
            *(bufp+screen->format->Bshift / 8) = b;
        }
        break;

        case 4: 
        { 
            // Probably 32-bpp
            Uint32 * bufp;
            bufp = (Uint32 *) screen->pixels + y * screen->pitch / 4 + x;
            *bufp = color;
        }
        break;
    }
    
    if (SDL_MUSTLOCK(screen)) 
    {
        SDL_UnlockSurface(screen);
    }
    
    //SDL_UpdateRect(screen, x, y, 1, 1);
}
