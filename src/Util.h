#ifndef _ARMRENDER_UTIL_H_
#define _ARMRENDER_UTIL_H_

#include <sys/time.h>
#include <unistd.h>
#include <SDL.h>

#include "Globals.h"

// Utility Methods
void * ConsumeWork(void * raytraceThreadInfo);
void * ProduceWork(void * pixelsPerThread);

void GetClosestIntersection(Vector rayOrigin, Vector rayDirection, SceneObject ** intersectedObject, Vector * intersection, float * intersectionTval);

unsigned int GetTriangleIntersection(Vector p0, Vector pd, Vector v0, Vector v1, Vector v2, float * intersect);
unsigned int GetSphereIntersection(Vector p0, Vector pd, Vector center, float radius, float * intersect1, float * intersect2);

void UpdateIntersection(SceneObject ** storedObject, float * storedTval, SceneObject * intersectedObject, float intersectionTval);

// Calculates the NORMALIZED reflection vector.
// Note: This assumes that 'vector' is pointing out from
//       the surface, and NOT into the surface!
Vector CalculateReflectedVector(Vector vector, Vector normal);

Vector GetColorForPixel(Uint32 xPixel, Uint32 yPixel);

void AddPhongColor(Vector * color, Vector rayDirection, Vector * reversedRayDirection, SceneObject * intersectedObject, Vector intersection, MaterialInfo * materialInfo, Vector normal);

void DrawPixel(SDL_Surface *screen, Uint8 R, Uint8 G, Uint8 B, Uint32 x, Uint32 y);

void PrintElapsedTime(struct timeval start, struct timeval end);

int FloatEqual(float f1, float f2);

#endif
