#include <math.h>

#include "Vector.h"

Vector NewVector(float x, float y, float z)
{
    Vector result;
    
    result.x = x;
    result.y = y;
    result.z = z;
    
    return result;
}

void NormalizeVector(Vector * v)
{
    float length = VectorMagnitude(*v);
    
    v->x /= length;
    v->y /= length;
    v->z /= length;
}

Vector GetNormalizedVector(Vector v)
{
    NormalizeVector(&v);
    return v;
}

void ScaleVector(Vector * v, float scale)
{
    v->x *= scale;
    v->y *= scale;
    v->z *= scale;
}

Vector GetScaledVector(Vector v, float scale)
{
    ScaleVector(&v, scale);
    return v;
}

float VectorMagnitude(Vector v)
{
    return sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
}

float DotProduct(Vector v1, Vector v2)
{
    return (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
}

float PositiveDotProduct(Vector v1, Vector v2)
{
    float dotProduct = DotProduct(v1, v2);
    
    return (dotProduct < 0.0f) ? 0.0f : dotProduct;
}

Vector CrossProduct(Vector v1, Vector v2)
{
    Vector result;
    
    result.x = (v1.y * v2.z) - (v1.z * v2.y);
    result.y = (v1.z * v2.x) - (v1.x * v2.z);
    result.z = (v1.x * v2.y) - (v1.y * v2.x);
    
    return result;
}

Vector SubtractVector(Vector v1, Vector v2)
{
    // v1 is passed by value, so changing it is OK.
    v1.x = v1.x - v2.x;
    v1.y = v1.y - v2.y;
    v1.z = v1.z - v2.z;
    
    return v1;
}

Vector AddVector(Vector v1, Vector v2)
{
    // v1 is passed by value, so changing it is OK.
    v1.x = v1.x + v2.x;
    v1.y = v1.y + v2.y;
    v1.z = v1.z + v2.z;
    
    return v1;
}
