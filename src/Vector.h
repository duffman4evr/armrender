#ifndef _ARMRENDER_VECTOR_H_
#define _ARMRENDER_VECTOR_H_

typedef struct 
{
    float x;
    float y;
    float z;
} 
Vector;

// Constructor.
Vector NewVector(float x, float y, float z);

// Normalize the vector (make it have length = 1).
void NormalizeVector(Vector * v);
Vector GetNormalizedVector(Vector v);

// Scale the vector.
void ScaleVector(Vector * v, float scale);
Vector GetScaledVector(Vector v, float scale);

// Get the magnitude (length) of the vector.
float VectorMagnitude(Vector v);

// Dot product.
float DotProduct(Vector v1, Vector v2);

// Positive Dot product (Sets negative values to 0.0f)
float PositiveDotProduct(Vector v1, Vector v2);

// Cross product.
Vector CrossProduct(Vector v1, Vector v2);

// Subtraction (does v1 - v2)
Vector SubtractVector(Vector v1, Vector v2);

// Addition (does v1 + v2)
Vector AddVector(Vector v1, Vector v2);

#endif
